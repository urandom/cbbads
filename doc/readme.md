## Design

- 2 PHP Skripte. Eines für Backend, eines als Frontend
- Konfiguration ohne DB

### Frontend

Da die Webmaster nur einmal gebeten werden sollen ein HTML Snippet einzubinden, soll sich die URL nicht ändern.
Die URL kann z.B. http://ccb.de/ads/uni_kiel heissen.
Wir mappen via mod_rewrite auf ein frontend.php Skript, welches als Parameter <uni_kiel> bekommt.
Das Skript ließt die Konfiguration für uni_kiel und liefert eine HTML Datei von der Festplatte aus.
Die HTML Datei wird statisch vorbereitet um kein parsing während der Anfrage durchführen zu müssen.

```php
$target = new Target($slug);
echo $target->render();
```

### Backend

Das Backend ist ein weiteres Skript, welches Konfigurationen verwaltet und fertige Snippets erzeugt.
Es ist per .htaccess abgesichert.

## Objekte

### Veranstaltung / Affair

Veranstaltungen enthalten Informationen wie

- Bild (URL)
- Text

### Target / Endpunkt

Ein Endpunkt ist eine URL über die eine externe Seite eine Anzeige bzw rotierende Anzeigen einbindet.

- Name
- Slug (Kürzel, bzw URL über die der Endpunkt angesprochen wird)
- Layout

```php
class Target {
   function __construct($slug) {
       // find configuration or die
   }
   function event() {

   }
   function render() {
      // read precompiled event
   }
}
```

### Verteilung

- Event
- Target
- Probability

### Layout

Ein Layout überführt ein Event in eine HTML-Datei.
Layouts liegen in einem speziellen Verzeichnis


## Projekt-Struktur

```
/public             #
/app                #
/app/models         #
```

## Backend GUI

### Navigation

- Endpunkte
- Veranstaltungen

#### Endpunkte

links: Nav mit allen Endpunkten + Button <neuer Endpunkt>
rechts: Formular für Endpunkt
  - name
  - slug
  - dropdown layout
  - button löschen

## Directory Layout
app/public
app/public/index.php              -> Entry for Public Widgets to Flight App, .htacces rewrites /widgets/target1 to this
app/public/backend/index.html     -> Angular Backend, Folder ist HTTP Basic AUTH Protected, SSL enforced
app/public/backend/api.php        -> API Entry, .htaccess rewrites /backend/api/... to this
app/flight                        -> PHP Flight App (common for public frontend and api)

app/                        -> DocumentRoot
app/.htaccess
app/index.html              -> NG
app/api.php
app/widgets.php

public/



https://github.com/mikecao/flight
http://yeoman.io/community-generators.html
http://www.thinkster.io/pick/GtaQ0oMGIl/a-better-way-to-learn-angularjs#item-51e7995d6646e9640500001d