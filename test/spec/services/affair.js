'use strict';

describe('Service: affair', function () {

  // load the service's module
  beforeEach(module('cbbadsApp'));

  // instantiate service
  var affair;
  beforeEach(inject(function (_affair_) {
    affair = _affair_;
  }));

  it('should do something', function () {
    expect(!!affair).toBe(true);
  });

});
