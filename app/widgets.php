<?php
require __DIR__ . '/../composer/mikecao/flight/flight/Flight.php';
Fligh::path(__DIR__ . '/../server/lib'); # Let the autoloaded know where to find class files.
Flight::set('flight.views.path', __DIR__ . '/../server/views');

Flight::route('/', function () {
    Flight::render('app', array(), 'body');
    Flight::render('layout', array());
});
Flight::route('GET /affairs', array('Affair_Controller', 'all'));
Flight::route('PUT /affair', array('Affair_Controller', 'create'));
Flight::route('GET /affair/@id', array('Affair_Controller', 'findOne'));
Flight::route('PUT /affair/@id', array('Affair_Controller', 'update'));
Flight::route('DELETE /affair/@id', array('Affair_Controller', 'remove'));
//RouteInsertReference
Flight::start();
