'use strict';

angular.module('cbbadsServices', ['ngResource'])
  .factory('affairStorage', ['$resource',
    function ($resource) {
    return $resource('/api/affairs/:id',
      { id: '@id', format: 'json'}
//      ,{ get: {method: 'GET', cache: true} }
    );
  }]);
