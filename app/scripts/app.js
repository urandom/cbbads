'use strict';

angular.module('cbbadsApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ngAnimate',
    'mgcrea.ngStrap',
    'cbbadsServices'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/affairs', {
        templateUrl: 'views/affairs.html',
        controller: 'AffairIndexCtrl'
      })
      .when('/affairs/:id', {
        templateUrl: 'views/affairs.html',
        controller: 'AffairIndexCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
