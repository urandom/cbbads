<?php

class Affair_Storage
{
    protected static $_db = NULL;
    protected static $_collection = NULL;

    protected static function _init()
    {
        self::$_collection = array(
          0 => array(
              'id' => 0,
              'name' => 'CoderWeeek'
          )
        );
//        if (!self::$_db) {
//            $mongo = new MongoClient();
//            self::$_db = $mongo->cbbadsbackend;
//        }
//        if (!self::$_collection) {
//            self::$_collection = self::$_db->affairs;
//        }
    }

    public static function all()
    {
        self::_init();
//        return array_values(iterator_to_array(self::$_collection->find()));
        return self::$_collection;
    }

    public static function create($data)
    {
        self::_init();
        self::$_collection->insert($data);
        return $data; // _id will be added by the insert and included in the return value.
    }

    public static function findOne($id)
    {
        self::_init();
        return self::$_collection->findOne(array('_id' => new MongoId($id)));
    }

    public static function update($id, $data)
    {
        self::_init();
        unset($data['_id']); // Don't try to rewrite the id
        $data['updated'] = new MongoDate(); // Auto add an updated timestamp
        self::$_collection->update(array('_id' => new MongoId($id)), $data);
        return self::findOne($id);
    }

    public static function remove($id)
    {
        self::_init();
        self::$_collection->remove(array('_id' => new MongoId($id)));
        return true;
    }
}
